# System Service

## Reboot

Reboot device.

  GET [http://server]/power/reboot

## Power-Off

Switch device off.

  GET [http://server]/power/off
