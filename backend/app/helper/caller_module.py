import inspect


def caller_module():
    frm = inspect.stack()[2]
    mod = inspect.getmodule(frm[0])
    split = mod.__name__.split(".")
    if len(split) >= 2:
        ret = split[1]
    else:
        ret = split[0]
    return ret
