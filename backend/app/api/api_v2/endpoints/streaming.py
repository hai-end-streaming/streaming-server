###############################################################################
#
#  SPDX-License-Identifier: AGPL-3.0-or-later
#  Hai-End Streaming
#  Copyright (C) 2022 Maik Ender and Carl-Daniel Hailfinger
#
###############################################################################
from typing import Optional  # dependecy python <3.10
from fastapi import APIRouter, Request, Response
from pydantic import BaseModel
import logging

from app.main_director.state_id import StateId

logger = logging.getLogger(__name__)

router = APIRouter()


###########################################################################
# Status
###########################################################################
@router.get("/status")
async def get_status(request: Request):
    return {"status": request.app.main_director.state[StateId.streaming]["current"]}


###########################################################################
# Stream Controlling
###########################################################################


@router.post("/start", status_code=204, response_class=Response)
async def stream_start(request: Request, test: Optional[bool] = False) -> None:
    request.app.main_director.start_pipeline_user(False, test)


@router.post("/receive", status_code=204, response_class=Response)
async def stream_start(request: Request) -> None:
    request.app.main_director.start_pipeline_user(True)


@router.post("/stop", status_code=204, response_class=Response)
async def stream_stop(request: Request) -> None:
    request.app.main_director.stop_pipeline_user()
