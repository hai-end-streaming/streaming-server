###############################################################################
#
#  SPDX-License-Identifier: AGPL-3.0-or-later
#  Hai-End Streaming
#  Copyright (C) 2022 Maik Ender and Carl-Daniel Hailfinger
#  Copyright (C) 2024 Carl-Daniel Hailfinger
#
###############################################################################
from typing import Optional, Union, List  # dependency python <3.10
from fastapi import APIRouter, Request, Query
from pydantic import BaseModel
from fastapi.responses import JSONResponse
from fastapi.encoders import jsonable_encoder
from fastapi.exceptions import RequestValidationError
import logging

logger = logging.getLogger(__name__)

router = APIRouter()


###########################################################################
# Property Controlling
###########################################################################
@router.get("/")
async def get_property_status(request: Request):
    return {
        "audio_source_devices": request.app.streaming_director.get_audio_device_names(
            True
        ),
        "audio_sink_devices": request.app.streaming_director.get_audio_device_names(
            False
        ),
        "hostname": request.app.streaming_director.hostname,
        "fqdn": request.app.streaming_director.fqdn,
    }


@router.get("/{config_name}")
async def get_property(config_name: str, request: Request):
    logger.debug("get config_name: {}".format(config_name))
    try:
        ret = request.app.settings[config_name]
    except AttributeError as e:
        logger.warning(f"AttributeError reading '{config_name}'")
        return JSONResponse(
            status_code=422,
            content=jsonable_encoder({"detail": e, "body": config_name}),
        )
    return {"return": ret}


@router.post("/{config_name}")
async def set_property(
    config_name: str,
    value: str,
    request: Request,
    valuelist: Union[List[str], None] = Query(default=None),
):
    # The semantics here are a bit weird:
    # If valuelist is provided, value is ignored.
    # FIXME: Find a nicer way to handle this
    if valuelist != None:
        logger.info(f"set config: '{config_name}' to '{valuelist}'")
    else:
        logger.info(f"set config: '{config_name}' to '{value}'")
    try:
        # https://fastapi.tiangolo.com/tutorial/handling-errors/
        if valuelist != None:
            valuelist = [a for a in valuelist if a not in ["", "None"]]
            logger.debug(f"cleaned valuelist '{valuelist}'")
            request.app.settings[config_name] = valuelist
        else:
            request.app.settings[config_name] = value
    except RequestValidationError as e:
        logger.warning(f"ValidationError '{config_name}' to '{value}'")
        return JSONResponse(
            status_code=422,
            content=jsonable_encoder({"detail": e.errors(), "body": e.body()}),
        )
    except ValueError as e:
        logger.warning(f"ValueError '{config_name}' to '{value}'")
        return JSONResponse(
            status_code=422,
            content=jsonable_encoder({"detail": str(e), "body": value}),
        )

    return {"return": True}
