###############################################################################
#
#  SPDX-License-Identifier: AGPL-3.0-or-later
#  Hai-End Streaming
#  Copyright (C) 2021 Maik Ender and Carl-Daniel Hailfinger
#
###############################################################################

import logging, logging.config
import asyncio

from app.settings.settings import Settings, global_config

# setup loggers very early
from app.settings.logging import config as logger_config

# Here we have to first reset all existing loggers to the root logger
# this is needed because
# uvicorn has its own logger.handler in place so that we could not configure it via
# our logging configuration file. It also propagate it to the root logger
# so we would see each line twice
# .
# Also we wanted to set a specific handler for the "uvicorn.error" handler
# as this is not meant for errors but for non "access" logs
# for this see: https://github.com/encode/uvicorn/issues/562
# our quick solution is a custom handler which does not return the name of the logger
# instead mimic the name by setting just uvicorn as text
for name in logging.root.manager.loggerDict.keys():
    logging.getLogger(name).handlers = []
    logging.getLogger(name).propagate = True

logging.config.fileConfig(logger_config, disable_existing_loggers=False)
logger = logging.getLogger(__name__)

from fastapi import FastAPI, Depends
from fastapi.middleware.cors import CORSMiddleware
from starlette.middleware.base import BaseHTTPMiddleware
from prometheus_fastapi_instrumentator import Instrumentator

from app.single_page_application import SinglePageApplication
from fastapi.staticfiles import StaticFiles
from app.api.api_v2.api import api_router
from app.api.api_v2.endpoints.ws import web_socket_manager
from app.api.portal.api import router as portal_api_router

from app.overlay.generator import OverlayGenerator
from app.main_director import MainDirector
from app.main_director.main_director_messages import MainDirectorMessages
from app.portal.director import PortalDirector

from app.version import STREAMING_SERVER_VERSION

app = FastAPI(
    title="Hai-End Streaming - Streaming",
    description="Streaming Server",
    version=STREAMING_SERVER_VERSION,
    contact={
        "name": "The Hai-End Streaming Project",
        "url": "https://hai-end-streaming.de/",
        "email": "hallo@hai-end-streaming.de",
    },
    license_info={
        "name": "AGPL-3.0-or-later",
        "url": "https://www.gnu.org/licenses/agpl-3.0.txt",
    },
)

app.settings = Settings().read_config()


loggers = [logging.getLogger(name) for name in logging.root.manager.loggerDict]
logger.info(loggers)


@app.on_event("startup")
async def app_startup():
    logger.info("app startup start")
    app.async_loop = asyncio.get_event_loop()
    main_director = MainDirector(app)
    portal_director = PortalDirector(app)
    overlay_generator = OverlayGenerator(app)
    web_socket_manager.set_app(app)
    # now as we are almost ready ask the portal if anything is todo
    main_director.send_message(MainDirectorMessages.RECOVER)
    logger.info("app startup completed")


@app.on_event("shutdown")
def shutdown_event():
    logger.info("app shutdown start")
    app.main_director.stop_glib_mainloop()
    logger.info("app shutdown completed")

class MiddleWare_CacheControle(BaseHTTPMiddleware):
    async def dispatch(self, request, call_next):
        response = await call_next(request)
        if request.url.path in ["/", "/index.html"]:
            response.headers["Cache-Control"] = "no-cache, no-store"
        return response

app.add_middleware(MiddleWare_CacheControle)

app.add_middleware(
    CORSMiddleware,
    allow_origins=app.settings["WebServer.allow_origins"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

if global_config.PUBLISH_METRICS:
    # add prometheus metrics
    Instrumentator().instrument(app).expose(app)


@app.get("/ping")
def read_root():
    return {"pong": "success"}


app.include_router(
    portal_api_router, prefix=global_config.PORTAL_PREFIX, tags=["PortalAPI"]
)
app.include_router(api_router, prefix=global_config.API_PREFIX)

logger.info(app.settings["WebServer.web_root"])

app.mount(
    "/overlayimages",
    StaticFiles(directory=app.settings["WebServer.overlay_root"]),
    name="overlayimages",
)

app.mount(
    path="/",
    app=SinglePageApplication(directory=app.settings["WebServer.web_root"]),
    name="SPA",
)
