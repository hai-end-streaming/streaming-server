from enum import IntEnum


class MediaStates(IntEnum):
    NULL = 1
    READY = 2
    STARTING = 3
    PLAYING = 4
    ERROR = 5
    RECOVER = 6
