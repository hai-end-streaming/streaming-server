from enum import IntEnum

# fmt: off
class MainDirectorMessages(IntEnum):
    STOP                = 1
    REQUEST_SEND_START  = 2
    REQUEST_RECV_START  = 3
    START_SEND          = 4
    START_REVC          = 5
    STOP_SEND           = 50
    STOP_RECV           = 51
    
    VOIP_READY          = 100
    VOIP_CALL_DIALING   = 101
    VOIP_CALL_ESTABLISHED = 102
    VOIP_CALL_STOPPED   = 103
    
    STREAM_STARTING     = 200
    STREAM_PLAYING      = 201
    STREAM_STOPPED      = 202

    RECOVER             = 500

    WARNING_QOS         = 800
    
    ERROR               = 900
    ERROR_NETWORK       = 901
    ERROR_DEVICE        = 902
    ERROR_ENCODER       = 903
# fmt: on
