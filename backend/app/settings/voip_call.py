from typing import List, Optional
from xmlrpc.client import boolean
from pydantic import BaseModel
from . import settings_class


@settings_class
class VoIPCall(BaseModel):
    # TODO: regex if phone numbers are
    dial_number: List[str] = [""]

    room_number: str = ""
    room_modpin: str = ""

    voip_activated: boolean = False

    class Config:
        validate_assignment = True
