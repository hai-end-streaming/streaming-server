from pydantic import BaseModel
from . import settings_class


@settings_class
class VoIPTwinkle(BaseModel):
    dev_ringtone: str = "alsa:plughw:0,1,1"
    dev_speaker: str = "alsa:plughw:0,1,1"
    dev_mic: str = "alsa:plughw:0,0,0"

    redial_url: str = ""
    redial_profile: str = ""

    class Config:
        validate_assignment = True
