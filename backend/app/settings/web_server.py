from pydantic import BaseModel
from typing import List
from . import settings_class


@settings_class
class WebServer(BaseModel):
    web_root: str = "../webui/dist"
    overlay_root: str = "../../overlay_img"
    allow_origins: List[str] = [
        "http://localhost",
        "http://localhost:8080",
    ]

    class Config:
        validate_assignment = True
        allow_mutation = False
