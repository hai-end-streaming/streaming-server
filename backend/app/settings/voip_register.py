from pydantic import BaseModel
from . import settings_class


@settings_class
class VoIPRegister(BaseModel):
    twinkle_profile: str = "FritzBox"
    user_name: str = ""
    user_domain: str = ""
    user_display: str = ""
    auth_name: str = ""
    auth_pass: str = ""
    registrar: str = ""
    register_at_startup: str = ""

    class Config:
        validate_assignment = True
