import configparser
import os

from .settings import global_config

config = configparser.ConfigParser()
config.read(global_config.LOGGING_CONFIG_FILE)
