from pydantic import BaseModel
from . import settings_class


@settings_class
class Overlay(BaseModel):
    end_card_text: str = "Danke, dass ihr dabei wart.\nDieser Stream ist nun zu Ende."
    default_hymn_book: str = "de_gesangbuch"
    
    class Config:
        validate_assignment = True
