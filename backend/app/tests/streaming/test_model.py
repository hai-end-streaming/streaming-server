import pytest
from app.streaming.director import *
from app.helper import is_linux

@pytest.fixture(scope='class')
def settings(request):
    from app.settings.settings import Settings
    return Settings().read_config()


@pytest.fixture(scope='class')
def streaming_director(request, settings):
    from fastapi import FastAPI
    app = FastAPI()
    app.settings = settings
    sut = StreamingDirector(app)
    # Needs more work to work
    sut.start_pipeline()
    yield sut
    sut.start_pipeline()

@pytest.mark.parametrize("prop,in1,expected1,in2,expected2", [
    ('volume', 6, 6.0, 5, 5.0),
    # note, sometimes the bitrate can only be a multiple of 1024
    ('video_bitrate', 1024000, 1024000, 2048000, 2048000 ),
    # needs running client
     pytest.param('text_overlay', 'BLA', 'BLA', 'BLUB', 'BLUB', marks=pytest.mark.xfail),
])
class TestStreamingDirectorProperties:
    def test_property_direct(self, prop, in1, expected1, in2, expected2, streaming_director):
        streaming_director.set_property(prop, in1)
        assert streaming_director.get_property(prop) == expected1
        streaming_director.set_property(prop, in2)
        assert streaming_director.get_property(prop) == expected2

    def test_settings_direct(self, prop, in1, expected1, in2, expected2, settings):
        settings[f"StreamingDirector.{prop}"] = in1
        assert settings[f"StreamingDirector.{prop}"] == expected1
        settings[f"StreamingDirector.{prop}"] = in2
        assert settings[f"StreamingDirector.{prop}"] == expected2

    def test_cross_ltr(self, prop, in1, expected1, in2, expected2, settings, streaming_director):
        settings[f"StreamingDirector.{prop}"] = in1
        assert streaming_director.get_property(prop) == expected1
        settings[f"StreamingDirector.{prop}"] = in2
        assert streaming_director.get_property(prop) == expected2

    @pytest.mark.xfail(reason="the property-api does not yet update the settings")
    def test_cross_rtl(self, prop, in1, expected1, in2, expected2, settings, streaming_director):
        streaming_director.set_property(prop, in1)
        assert settings[f"StreamingDirector.{prop}"] == expected1
        streaming_director.set_property(prop, in2)
        assert settings[f"StreamingDirector.{prop}"] == expected2
