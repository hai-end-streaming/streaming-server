/*
  SPDX-License-Identifier: AGPL-3.0-or-later
  Hai-End Streaming - Streaming Server Frontend
  Copyright (C) 2021 Stephan Strittmatter
*/
import { defineStore } from 'pinia'

export const useTemplatesStore = defineStore('templates', {

  state: () => ({
    hymnList: [],
    templateList: []
  }),
  actions: {
    loadTemplates () {
      const templates = localStorage.getItem('haiend.text-templates')
      if (templates) {
        this.templateList = JSON.parse(templates)
      } else {
        this.templateList = []
      }
    },
    saveTemplates () {
      if (this.templateList.length > 0) {
        window.localStorage.setItem('haiend.text-templates', JSON.stringify(this.templateList))
      } else {
        window.localStorage.removeItem('haiend.text-templates')
      }
    },
    addTemplate (template) {
      console.log('addTemplate', template)
      if (template) {
        this.templateList.push(template)
      }
      this.saveTemplates()
    },
    removeTemplate (index) {
      this.templateList.splice(index, 1)
      this.saveTemplates()
    },
    loadHymnList () {
      const templates = localStorage.getItem('haiend.hymn-templates')
      if (templates) {
        this.hymnList = JSON.parse(templates)
      } else {
        this.hymnList = []
      }
    },
    saveHymnList () {
      if (this.hymnList.length > 0) {
        window.localStorage.setItem('haiend.hymn-templates', JSON.stringify(this.hymnList))
      } else {
        window.localStorage.removeItem('haiend.hymn-templates')
      }
    },
    addHymn (hymn) {
      this.hymnList.push(hymn)
      this.saveHymnList()
    },
    removeHymn (index) {
      this.hymnList.splice(index, 1)
      this.saveHymnList()
    }
  }
})
