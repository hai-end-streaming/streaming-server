/*
  SPDX-License-Identifier: AGPL-3.0-or-later
  Hai-End Streaming - Streaming Server Frontend
  Copyright (C) 2021 Stephan Strittmatter
*/
import streamingService from '@/services/streaming.service'
import mediaStates from '@/types/mediaStates'

import { defineStore } from 'pinia'

const DIRECTION_TRANSMIT = 'transmit'
const DIRECTION_RECEIVE = 'receive'

export const useStreamingStore = defineStore('streaming', {

  state: () => ({
    streaming: {
      current: 'NULL',
      intended: 'NULL',
      direction: null
    },
    voip: {
      current: 'NULL',
      intended: 'NULL'
    },
    streamingError: null,
    bitrate: null,
    resolution: null
  }),
  getters: {
    streamingStatus: (state) => {
      return state.streaming ? state.streaming : { current: mediaStates.NULL, intended: mediaStates.NULL }
    },
    isPlayingStream (state) {
      if (mediaStates.PLAYING === state.streaming?.current || mediaStates.PLAYING === state.streaming?.intended) {
        return true
      } else {
        return false
      }
    },
    isActiveTransmitStream (state) {
      if (this.isPlayingStream && state.streaming.direction === DIRECTION_TRANSMIT) {
        return true
      } else {
        return false
      }
    },
    isActiveReceiveStream (state) {
      if (this.isPlayingStream && state.streaming.direction === DIRECTION_RECEIVE) {
        return true
      } else {
        return false
      }
    },
    voipStatus: (state) => {
      return state.voip ? state?.voip : { current: mediaStates.NULL, intended: mediaStates.NULL }
    },
    streamingDirection: (state) => {
      return state.streaming?.direction
    }
  },
  actions: {
    setServerStatus (status) {
      if (status?.streaming) {
        this.streaming = status.streaming
      }
      if (status?.voip) {
        this.voip = status.voip
      }
    },
    startStream (flag) {
      streamingService.startStream(flag)
    },
    stopStream () {
      streamingService.stopStream()
    },
    receiveStream () {
      streamingService.receiveStream()
    },
    setStreamingError (streamingError) {
      this.streamingError = streamingError
    },
    setBitrate (bitrate) {
      this.bitrate = bitrate
    },
    setResolution (resolution) {
      this.resolution = resolution
    }
  }
})
