/*
  SPDX-License-Identifier: AGPL-3.0-or-later
  Hai-End Streaming - Streaming Server Frontend
  Copyright (C) 2021 Stephan Strittmatter
*/
const preferenceService = {
  load: () => {
    // return api.restClient().get('/preferences')
    const preferences = localStorage.getItem('haiend.preferences')
    let retval
    if (preferences) {
      retval = JSON.parse(preferences)
    } else {
      retval = {
        overlay: true
      }
    }
    return retval
  },
  save: (preferences) => {
    // return api.restClient().post('/preferences', preferences)
    localStorage.setItem('haiend.preferences', JSON.stringify(preferences))
  }
}

export default preferenceService
